#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(void)
{
	int pipes[2];
	int pid;
	char buf;
	pipe(pipes);
	pid=fork()
	if(pid !=0)
	{
		close(pipes[0]);
		char msg[255];
		printf("\n(Proces macierzysty) Podaj wiadomosc: ");
		scanf("%s", msg);
		write(pipes[1], msg, strlen(msg));
		close(pipes[1]);
	}
	if(pid ==0)
	{
		close(pipes[1]);
		while(read(pipes[0], &buf,1)<=0)
		printf("\n(proces potomny) Odebrano wiadomosc: %c",toupper(buf));
		while(read(pipes[0], &buf, 1)>0) printf("%c",toupper(buf));
		printf("\n\n");
		close(pipes[0]);
	}
	return 0;
}
