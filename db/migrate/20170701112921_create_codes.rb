class CreateCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :codes do |t|
      t.string :name
      t.string :attachment
      t.string :output
      t.text :error
      t.boolean :is_compiled, :default => false

      t.timestamps
    end
  end
end
