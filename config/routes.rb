Rails.application.routes.draw do
  devise_for :users
  #get 'codes/home'

  
  #get 'codes/new'

  #get 'codes/create'

  #get 'codes/destroy'

  #get 'codes/compile'

  resources :codes, only: [:home, :new, :create, :destroy, :compile, :download,:sendFile]
  get 'codes', to: "codes#home"
  root "codes#home"
  post 'codes/:id', to: 'codes#compile'
  post 'codes/download/:id', to: 'codes#sendFile'
  post 'codes/error/:id', to: 'codes#error'
  post 'codes/show/:id', to: 'codes#show'
  #post  'download/:id', to: 'download#sendFile'
  #get 'codes/:id', to: 'codes#compile'
  #get 'public/outputs/:output'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
