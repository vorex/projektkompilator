class CodesController < ApplicationController
  def home
      @codes = Code.all

   end
   
   def new
      @code = Code.new
   end
   
   def create
      @code = Code.new(code_params)
      
      if @code.save
        @code.update_attribute(:user, current_user.id)
         redirect_to codes_path, notice: "The code #{@code.name} has been uploaded."
      else
         render "new"
      end
      
   end
   
   def destroy
      @code = Code.find(params[:id])
      @code.destroy
      redirect_to codes_path, notice:  "The code #{@code.name} has been deleted."
   end
   
   def compile
      code= Code.find(params[:id])
      fileLocation=code.attachment.to_s
      output = `g++ -o public/outputs/output#{code.id} #{Rails.root}/public#{fileLocation} 2>&1`

      if output.empty? 
        code.update_attribute(:is_compiled, true)
        code.update_attribute(:output,"output#{code.id}")
      else 
        code.update_attribute(:error,output)
      end 
         
    end

    def sendFile
      code = Code.find(params[:id])
      send_file("#{Rails.root}/public/outputs/#{code.output}",filename: "#{code.name}")
    end

    def error
      @code = Code.find(params[:id])

    end

    def show
      @code = Code.find(params[:id])
    end
   


   private
      def code_params
      params.require(:code).permit(:name, :attachment, :is_compiled, :output, :error, :user)
   end
end
