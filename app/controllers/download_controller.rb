class DownloadController < ApplicationController
	 def sendFile
      code = Code.find(params[:id])
      send_file("#{Rails.root}/public/outputs/#{code.output}",filename: "#{code.name}",type: "application")
    end
end
